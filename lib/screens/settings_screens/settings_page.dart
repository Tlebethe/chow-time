import 'package:chow_time/constants/colors.dart';
import 'package:chow_time/services/auth/auth_provider.dart';
import 'package:chow_time/services/auth/firebase_auth_provider.dart';
import 'package:chow_time/services/firebase/firebase_crud.dart';
import 'package:flutter/material.dart';

import '../../constants/routes.dart';
import '../../services/auth/auth_service.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({super.key});

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            'Settings',
            style: TextStyle(
              fontSize: 30,
              color: coolGreen,
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          const Text(
            'Account',
            style: TextStyle(
              color: coolGreen,
              fontSize: 18,
            ),
          ),
          //rounded image in center
          // user can upload a propic
          const SizedBox(
            height: 20,
          ),
          Center(
            child: CircleAvatar(
              backgroundColor: coolGreen,
              radius: 40,
              child: IconButton(
                  onPressed: () {
                    print('change pro pic');
                  },
                  icon: const Icon(
                    Icons.camera_alt,
                    color: coolWhite,
                  )),
            ),
          ),

          //row
          //username -> button to update username
          const SizedBox(
            height: 80,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text(
                'Update username',
                style: TextStyle(
                  color: coolGrey,
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                ),
              ),
              IconButton(
                  onPressed: () {
                    debugPrint('update username?');
                    showAboutDialog(context: context);
                  },
                  icon: const Icon(
                    Icons.update,
                    size: 35,
                    color: coolGreen,
                  )),
            ],
          ),
          const SizedBox(
            height: 20,
          ),

          //create a dish
          //button that takes me to create dish page
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text(
                'Create a recipe? ',
                style: TextStyle(
                  color: coolGrey,
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                ),
              ),
              IconButton(
                  onPressed: () async {
                    debugPrint('update username?');
                    //await FirebaseCrud().updateUsername('Thato');
                  },
                  icon: const Icon(
                    Icons.food_bank_outlined,
                    size: 35,
                    color: coolGreen,
                  )),
            ],
          ),

          //logout button
          const SizedBox(
            height: 80,
          ),
          Center(
            child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(coolGreen),
              ),
              child: const Text('Logout'),
              onPressed: () {
                Navigator.pushNamedAndRemoveUntil(
                    context, loginView, (route) => false);
                AuthService.firebase().logout();
              },
            ),
          )
        ],
      ),
    );
  }
}

//on the settings page
//i should be able to update my name
//update my profile picture
//create a dish
//logout


