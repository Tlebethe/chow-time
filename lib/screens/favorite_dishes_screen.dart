import 'package:chow_time/constants/colors.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../models/favorite_recipe_model.dart';
import '../utilities/widgets.dart';

class FavoriteDishes extends StatefulWidget {
  const FavoriteDishes({super.key});

  @override
  State<FavoriteDishes> createState() => _FavoriteDishesState();
}

class _FavoriteDishesState extends State<FavoriteDishes> {
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            const Text(
              'Your favorite Dishes...',
              style: TextStyle(
                fontSize: 30,
                color: coolGreen,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Consumer<RecipeModel>(
              builder: (context, value, child) => Expanded(
                child: ListView.builder(
                    itemCount: value.getMyDishes.length,
                    itemBuilder: ((context, index) => favoriteMealCard(
                          context,
                          value.getMyDishes[index].strMeal.toString(),
                          value.getMyDishes[index].strMealThumb.toString(),
                          value.getMyDishes[index].idMeal.toString(),
                          value,
                        ))),
              ),
            ),
          ],
        ));
  }
}
