import 'package:chow_time/constants/colors.dart';
import 'package:flutter/material.dart';

class DishesWall extends StatefulWidget {
  const DishesWall({super.key});

  @override
  State<DishesWall> createState() => _DishesWallState();
}

class _DishesWallState extends State<DishesWall> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: const [
        Text(
          'Recipe Feed',
          style: TextStyle(
            fontSize: 30,
            color: coolGreen,
          ),
        ),
        SizedBox(
          height: 30,
        ),
        Text(
          'Create and post recipes?',
          style: TextStyle(
            fontSize: 18,
            color: coolGrey,
          ),
        ),
        SizedBox(
          height: 30,
        ),
        Text(
          'feature coming soon....',
          style: TextStyle(
            fontSize: 25,
            color: coolGreen,
          ),
        ),
        Center(
          child: Image(
            image: AssetImage('images/underworks.jpg'),
            width: 400,
            height: 400,
          ),
        ),
      ],
    );
  }
}
