import 'package:chow_time/constants/colors.dart';
import 'package:chow_time/models/dish.dart';
import 'package:chow_time/models/favorite_recipe_model.dart';
import 'package:chow_time/screens/cooking_instructions_screen.dart';
import 'package:chow_time/services/food_api/mealdb_api_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../utilities/widgets.dart';

class RecipeDetailScreen extends StatefulWidget {
  final String id;

  const RecipeDetailScreen({super.key, required this.id});

  @override
  State<RecipeDetailScreen> createState() => _RecipeDetailScreenState();
}

class _RecipeDetailScreenState extends State<RecipeDetailScreen> {
  bool isLiked = false;
  void changeIsLiked() {
    setState(() {
      isLiked = !isLiked;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<RecipeModel>(
      builder: (context, value, child) => Scaffold(
        bottomNavigationBar: SizedBox(
          height: 80,
          child: Row(
            children: [
              IconButton(
                onPressed: () async {
                  //call the data
                  final data = await MealDBApi().getDishDetail(widget.id);

                  final dish = Dish(
                    idMeal: data[0]['meals'][0]['idMeal'],
                    strMeal: data[0]['meals'][0]['strMeal'],
                    strCategory: data[0]['meals'][0]['strCategory'],
                    strInstructions: data[0]['meals'][0]['strInstructions'],
                    strMealThumb: data[0]['meals'][0]['strMealThumb'],
                  );
                  //add the data to dishes class
                  value.add(dish);

                  changeIsLiked();
                },
                icon: Icon(
                  Icons.favorite,
                  color: isLiked ? Colors.red : coolGrey,
                  size: 30,
                ),
              ),
              InkWell(
                onTap: () {
                  debugPrint('show me instructions');
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: ((context) {
                    return StartCookingScreen(
                      mealID: widget.id,
                    );
                  })));
                },
                child: Container(
                  padding: const EdgeInsets.all(15),
                  width: MediaQuery.of(context).size.width / 1.2,
                  decoration: BoxDecoration(
                    color: coolGreen,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: const Text(
                    'Start Cooking',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: coolWhite,
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        body: FutureBuilder(
          future: MealDBApi().getDishDetail(widget.id.toString()),
          builder: (context, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
                return const Text('No data please refresh');
              case ConnectionState.waiting:
                return const Center(
                  child: CircularProgressIndicator(),
                );
              case ConnectionState.active:
                return const Text('connected');
              case ConnectionState.done:
                return SafeArea(
                  child: Padding(
                    padding: const EdgeInsets.only(
                      left: 25,
                      right: 25,
                      top: 10,
                      bottom: 10,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        //header row
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            IconButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              icon: const Icon(
                                Icons.arrow_back,
                                size: 35,
                              ),
                            ),
                            const Text(
                              'Recipe Detail',
                              style: TextStyle(
                                fontSize: 20,
                                color: coolGrey,
                              ),
                            ),
                            IconButton(
                              onPressed: () {},
                              icon: const Icon(
                                Icons.more_vert,
                                size: 35,
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        //,dish image
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.black,
                            borderRadius: BorderRadius.circular(10),
                            image: DecorationImage(
                              opacity: 0.9,
                              image: NetworkImage(snapshot.data![0]['meals'][0]
                                  ['strMealThumb']),
                              fit: BoxFit.cover,
                            ),
                          ),
                          height: 200,
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        //name of dish
                        Text(
                          snapshot.data![0]['meals'][0]['strMeal'],
                          style: const TextStyle(
                            fontSize: 30,
                            fontWeight: FontWeight.w700,
                            color: coolGrey,
                          ),
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        // category
                        Text(
                          '${snapshot.data![0]["meals"][0]["strCategory"]}',
                          style: const TextStyle(
                            fontSize: 20,
                            color: coolGreen,
                          ),
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                        //ingredients
                        //row
                        //item and quantity
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text(
                              'Ingredients',
                              style: TextStyle(
                                fontSize: 25,
                                color: coolGrey,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: const [
                                    Text(
                                      '6',
                                      style: TextStyle(
                                        fontSize: 22.5,
                                        color: coolGrey,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      'items',
                                      style: TextStyle(
                                        fontSize: 22.5,
                                        color: coolGrey,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 30,
                        ),

                        Expanded(
                          child: ListView.builder(
                            itemCount: 20,
                            itemBuilder: ((context, index) {
                              if (snapshot.data![0]['meals'][0]
                                      ['strIngredient${index + 1}'] !=
                                  '') {
                                return ingredientWidget(
                                  context: context,
                                  ingridentItem: snapshot.data![0]['meals'][0]
                                      ['strIngredient${index + 1}'],
                                  ingredientMeasure: snapshot.data![0]['meals']
                                      [0]['strMeasure${index + 1}'],
                                );
                              }
                              return const SizedBox();
                            }),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
            }
          },
        ),
      ),
    );
  }
}

