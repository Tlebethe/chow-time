import 'package:chow_time/constants/colors.dart';
import 'package:chow_time/constants/routes.dart';
import 'package:chow_time/services/auth/auth_service.dart';
import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  const Login({super.key});

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  late TextEditingController _emailController;
  late TextEditingController _passwordController;

  @override
  void initState() {
    super.initState();
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                const Center(
                  child: Image(
                    image: AssetImage('images/image2.jpg'),
                    width: 200,
                    height: 200,
                  ),
                ),
                const Text(
                  'Welcome Back to',
                  style: TextStyle(
                    fontSize: 40,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const Text(
                  'Foodie Friends..',
                  style: TextStyle(
                    fontSize: 35,
                    fontWeight: FontWeight.bold,
                    color: coolGreen,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                const Text(
                  'Login to your account',
                  style: TextStyle(
                    fontSize: 18,
                    color: coolGrey,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                TextField(
                  controller: _emailController,
                  decoration: const InputDecoration(
                    hintText: 'Enter your email',
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                TextField(
                  controller: _passwordController,
                  decoration: const InputDecoration(
                    hintText: 'Enter your password',
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(coolGreen),
                      ),
                      onPressed: () async {
                        String email = _emailController.text;
                        String password = _passwordController.text;

                        try {
                          await AuthService.firebase().login(email, password);
                          final user = AuthService.firebase().getCurrentUser;
                          if (user != null) {
                            if (user.isVerfied) {
                              if (mounted) {
                                Navigator.pushNamedAndRemoveUntil(
                                    context, feedView, (route) => false);
                              }
                            } else {
                              if (mounted) {
                                showErrorDialog(context, 'user not verified');
                              }
                            }
                          }
                        } catch (e) {
                          showErrorDialog(context, e.toString());
                        }
                      },
                      child: const Text('login'),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text(
                          'Not yet registered?',
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                        TextButton(
                          onPressed: () {
                            Navigator.pushNamedAndRemoveUntil(
                                context, registerView, (route) => false);
                          },
                          child: const Text(
                            'register',
                            style: TextStyle(
                              color: coolGreen,
                              fontSize: 20,
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

Future<void> showErrorDialog(BuildContext context, String title) {
  return showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        title: const Text('Error Occurred'),
        content: Text(title),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: const Text('OK'),
          ),
        ],
      );
    },
  );
}
