import 'package:chow_time/screens/auth_screens/login.dart';
import 'package:flutter/material.dart';

class SendEmailVerification extends StatefulWidget {
  const SendEmailVerification({super.key});

  @override
  State<SendEmailVerification> createState() => _SendEmailVerificationState();
}

class _SendEmailVerificationState extends State<SendEmailVerification> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Verify Email'),
      ),
      body: Column(
        children: [
          const Text('We have sent you an email verfication'),
          const SizedBox(
            height: 20,
          ),
          const Text('Please check your email then click restart to login'),
          TextButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Login(),
                  ),
                );
              },
              child: const Text('Restart')),
        ],
      ),
    );
  }
}
