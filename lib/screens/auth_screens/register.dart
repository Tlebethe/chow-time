import 'package:chow_time/constants/colors.dart';
import 'package:chow_time/constants/routes.dart';
import 'package:chow_time/services/auth/auth_service.dart';
import 'package:flutter/material.dart';
import 'login.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({super.key});

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  late TextEditingController _emailController;
  late TextEditingController _passwordController;
  late TextEditingController _userNameController;

  @override
  void initState() {
    super.initState();
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
    _userNameController = TextEditingController();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _userNameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                const Center(
                  child: Image(
                    image: AssetImage('images/image2.jpg'),
                    width: 200,
                    height: 200,
                  ),
                ),
                const Text(
                  "Looking for awesome , yummy recipes?",
                  style: TextStyle(
                    color: coolGreen,
                    fontSize: 30,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                const Text(
                  "Let's get you started...",
                  style: TextStyle(
                    fontSize: 18,
                    color: coolGrey,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                const Text(
                  'Register Account',
                  style: TextStyle(
                    fontSize: 40,
                    fontWeight: FontWeight.bold,
                    color: coolGrey,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                TextField(
                  controller: _userNameController,
                  decoration: const InputDecoration(
                    hintText: 'username',
                  ),
                ),
                TextField(
                  controller: _emailController,
                  decoration: const InputDecoration(
                    hintText: 'email address',
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                TextField(
                  controller: _passwordController,
                  decoration: const InputDecoration(
                    hintText: 'password',
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(coolGreen),
                      ),
                      onPressed: () async {
                        String email = _emailController.text;
                        String displayName = _userNameController.text;
                        String password = _passwordController.text;
                        try {
                          await AuthService.firebase().register(
                            email,
                            password,
                            displayName,
                          );
                          if (mounted) {
                            Navigator.pushNamedAndRemoveUntil(
                                context, verifyEmail, (route) => false);
                            AuthService.firebase().sendEmailVerifaction();
                          }
                        } catch (e) {
                          showErrorDialog(context, e.toString());
                        }
                      },
                      child: const Text('Register'),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text(
                          'Already registred?',
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                        TextButton(
                          onPressed: () {
                            Navigator.pushNamedAndRemoveUntil(
                                context, loginView, (route) => false);
                          },
                          child: const Text(
                            'click to login',
                            style: TextStyle(
                              color: coolGreen,
                              fontSize: 20,
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
