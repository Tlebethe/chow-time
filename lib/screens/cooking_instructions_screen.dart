import 'package:chow_time/services/food_api/mealdb_api_provider.dart';
import 'package:flutter/material.dart';

import '../constants/colors.dart';

class StartCookingScreen extends StatefulWidget {
  final String mealID;
  const StartCookingScreen({super.key, required this.mealID});

  @override
  State<StartCookingScreen> createState() => _StartCookingScreenState();
}

class _StartCookingScreenState extends State<StartCookingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(
            left: 25,
            right: 25,
            top: 10,
            bottom: 10,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //header row
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: const Icon(
                      Icons.arrow_back,
                      size: 35,
                    ),
                  ),
                  const Text(
                    'Start Cooking',
                    style: TextStyle(
                      fontSize: 20,
                      color: coolGrey,
                    ),
                  ),
                  const SizedBox(),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              const Text(
                'Lets cook some ...',
                style: TextStyle(
                  fontSize: 35,
                  color: coolGreen,
                ),
              ),
              const SizedBox(
                height: 20,
              ),

              FutureBuilder(
                future: MealDBApi().getDishDetail(widget.mealID),
                builder: (context, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                      return const Text('no data ,please refresh');
                    case ConnectionState.waiting:
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    case ConnectionState.active:
                      return const Text('connected');
                    case ConnectionState.done:
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            snapshot.data![0]['meals'][0]['strMeal'],
                            style: const TextStyle(
                              fontSize: 20,
                              color: coolGrey,
                            ),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          Center(
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              height: 180,
                              decoration: BoxDecoration(
                                color: Colors.black,
                                borderRadius: BorderRadius.circular(20),
                                image: DecorationImage(
                                  opacity: 0.5,
                                  fit: BoxFit.cover,
                                  image: NetworkImage(
                                    snapshot.data![0]['meals'][0]
                                        ['strMealThumb'],
                                  ),
                                ),
                              ),
                              child: IconButton(
                                  onPressed: () {},
                                  icon: const Icon(
                                    Icons.play_circle,
                                    color: coolWhite,
                                    size: 60,
                                  )),
                            ),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          const Text(
                            'Instructions',
                            style: TextStyle(
                              fontSize: 25,
                              color: coolGreen,
                            ),
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.38,
                            child: SingleChildScrollView(
                              child: Text(
                                snapshot.data![0]['meals'][0]
                                    ['strInstructions'],
                                style: const TextStyle(
                                  fontSize: 20,
                                  color: coolGrey,
                                  overflow: TextOverflow.fade,
                                  letterSpacing: 0.7,
                                ),
                                textDirection: TextDirection.ltr,
                              ),
                            ),
                          ),
                        ],
                      );
                  }
                },
              ),
              //name
              //video player
              //horizontal slide of ingredients
              //text ingredients
            ],
          ),
        ),
      ),
    );
  }
}
