import 'package:chow_time/constants/routes.dart';
import 'package:chow_time/models/favorite_recipe_model.dart';
import 'package:chow_time/screens/dishes_wall.dart';
import 'package:chow_time/screens/favorite_dishes_screen.dart';
import 'package:chow_time/screens/settings_screens/settings_page.dart';
import 'package:chow_time/services/auth/auth_service.dart';
import 'package:chow_time/services/food_api/api_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../constants/colors.dart';
import '../utilities/widgets.dart';

class RecipeFeed extends StatefulWidget {
  const RecipeFeed({super.key});

  @override
  State<RecipeFeed> createState() => _RecipeFeedState();
}

class _RecipeFeedState extends State<RecipeFeed> {
  String? name = AuthService.firebase().getCurrentUser?.userName;
  int index = 0;
  String feedChoice = 'beef';
  List categoryChoice = [
    {
      'category': 'beef',
      'isSelected': true,
    },
    {'category': 'chicken', 'isSelected': false},
    {'category': 'dessert', 'isSelected': false},
    {'category': 'pasta', 'isSelected': false},
    {'category': 'vegan', 'isSelected': false},
  ];

  String changeFeedChoice(String choice) {
    setState(() {
      feedChoice = choice;
    });
    return feedChoice;
  }

  void isChoosen(int index) {
    setState(() {
      for (int i = 0; i < categoryChoice.length; i++) {
        categoryChoice[i]['isSelected'] = false;
      }
      categoryChoice[index]['isSelected'] = true;
    });
  }

  int handleIndexChange(int num) {
    setState(() {
      index = num;
    });
    return index;
  }

  Widget viewChange(
    int index,
  ) {
    if (index == 0) {
      return homeFeed(
        name: name,
        context: context,
        categoryChoice: categoryChoice,
        feedChoice: feedChoice,
        isChoosen: isChoosen,
        changeFeedChoice: changeFeedChoice,
      );
    } else if (index == 1) {
      return const FavoriteDishes();
    } else if (index == 2) {
      return const DishesWall();
    } else if (index == 3) {
      return const SettingsPage();
    } else {
      return homeFeed(
          name: name,
          context: context,
          categoryChoice: categoryChoice,
          feedChoice: feedChoice,
          isChoosen: isChoosen,
          changeFeedChoice: changeFeedChoice);
    }
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => RecipeModel(),
      child: Scaffold(
        bottomNavigationBar: BottomNavigationBar(
          selectedItemColor: coolGreen,
          selectedFontSize: 18,
          unselectedFontSize: 13,
          elevation: 2.0,
          currentIndex: index,
          onTap: handleIndexChange,
          type: BottomNavigationBarType.fixed,
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.favorite),
              label: 'Favourite',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.cases_outlined),
              label: 'Wall',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.settings),
              label: 'Settings',
            ),
          ],
        ),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(25.0),
            child: Consumer<RecipeModel>(
              builder: (context, value, child) => viewChange(index),
            ),
          ),
        ),
      ),
    );
  }
}

Column homeFeed({
  required name,
  required context,
  required categoryChoice,
  required feedChoice,
  required isChoosen,
  required changeFeedChoice,
}) {
  return Column(
    children: [
      //app bar column
      //row
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Hello ,${name!.isNotEmpty ? name : 'no name'}👋',
                  style: const TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: coolGreen,
                  )),
              const SizedBox(
                height: 15,
              ),
              const Text(
                'What do you want to cook today?',
                style: TextStyle(
                  color: coolGrey,
                  fontSize: 18,
                ),
              )
            ],
          ),
          InkWell(
            //temp logout for now
            onTap: () {
              Navigator.pushNamedAndRemoveUntil(
                  context, loginView, (route) => false);
              AuthService.firebase().logout();
            },
            child: const CircleAvatar(
              radius: 25,
              backgroundColor: coolGrey,
              child: Text(
                'T',
                style: TextStyle(),
              ),
            ),
          ),
        ],
      ),
      const SizedBox(
        height: 25,
      ),
      //search bar
      Container(
        padding: const EdgeInsets.all(10),
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          color: Colors.black12,
        ),
        child: const TextField(
          decoration: InputDecoration(
            icon: Icon(
              Icons.search,
              size: 30,
            ),
            hintText: 'Search by name',
            border: InputBorder.none,
            hintStyle: TextStyle(
              fontSize: 18,
              color: coolGrey,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
      ),
      const SizedBox(
        height: 20,
      ),

      //category row
      const SizedBox(
        height: 15,
      ),
      Container(
        height: 60,
        color: coolWhite,
        child: ListView.builder(
          itemCount: categoryChoice.length,
          itemBuilder: (context, index) => categoryBarItem(
            categoryChoice[index]['category'],
            changeFeedChoice,
            categoryChoice[index]['isSelected'],
            isChoosen,
            index,
          ),
          scrollDirection: Axis.horizontal,
        ),
      ),
      const SizedBox(
        height: 20,
      ),
      //column recipes here
      Expanded(
        child: FutureBuilder<List>(
          future: ApiService.mealdb().getDishCategory(feedChoice),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              // use snapshot.data to access the API data
              return ListView.builder(
                itemBuilder: ((context, index) {
                  // print(snapshot.data![0]['meals']![index]['idMeal']);
                  return Consumer<RecipeModel>(
                    builder: (context, value, child) => mealCard(
                      context,
                      snapshot.data![0]['meals']![index]['strMeal'].toString(),
                      snapshot.data![0]['meals']![index]['strMealThumb'],
                      snapshot.data![0]['meals']![index]['idMeal'],
                      value,
                    ),
                  );
                }),
                itemCount: snapshot.data![0]['meals'].length,
              );
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }
            return const Center(
              child: SizedBox(
                child: CircularProgressIndicator(),
              ),
            );
          },
        ),
      ),

      //bottom tab bar
    ],
  );
}
