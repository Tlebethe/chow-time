import 'package:chow_time/services/food_api/food_api_provider.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class MealDBApi implements FoodApiProvider {
  @override
  Future<List> getData(String uri) async {
    var url = uri;

    try {
      var response = await http.get(Uri.parse(url));
      final json = jsonDecode(response.body);

      return [json];
    } catch (e) {
      return [e];
    }
    //print(response.body);
  }

  @override
  Future<List> getDishCategory(
    String category,
  ) async {
    String uri =
        'https://www.themealdb.com/api/json/v1/1/filter.php?c=$category';

    return await getData(uri);
  }

  @override
  Future<List> getDishDetail(String id) async {
    String uri = 'https://www.themealdb.com/api/json/v1/1/lookup.php?i=$id';
    return await getData(uri);
  }

  @override
  Future<List> getDishFeed(Uri uri) {
    throw UnimplementedError();
  }

  @override
  Future<List> getDishOftheDay() async {
    String uri = 'https://www.themealdb.com/api/json/v1/1/random.php';
    return await getData(uri);
  }

  @override
  Future<List> getSearchResult(Uri uri) {
    throw UnimplementedError();
  }
}
