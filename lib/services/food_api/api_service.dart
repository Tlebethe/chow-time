import 'package:chow_time/services/food_api/food_api_provider.dart';
import 'package:chow_time/services/food_api/mealdb_api_provider.dart';

class ApiService implements FoodApiProvider {
  FoodApiProvider provider;
  ApiService({required this.provider});

  factory ApiService.mealdb() => ApiService(
        provider: MealDBApi(),
      );

  @override
  Future<List> getData(String uri) => provider.getData(uri);

  @override
  Future<List> getDishCategory(String url) => provider.getDishCategory(url);

  @override
  Future<List> getDishDetail(String id) => provider.getDishDetail(id);

  @override
  Future<List> getDishFeed(Uri uri) => provider.getDishFeed(uri);

  @override
  Future<List> getDishOftheDay() => provider.getDishOftheDay();

  @override
  Future<List> getSearchResult(Uri uri) => getSearchResult(uri);
}
