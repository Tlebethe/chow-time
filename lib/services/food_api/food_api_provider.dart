abstract class FoodApiProvider {
  Future<List> getData(String uri);

  Future<List> getDishOftheDay();

  Future<List> getDishFeed(Uri uri);

  Future<List> getDishDetail(String id);

  Future<List> getSearchResult(Uri uri);

  Future<List> getDishCategory(String url);
}
