import 'package:chow_time/services/auth/auth_provider.dart';
import 'package:chow_time/services/auth/auth_user.dart';
import 'package:chow_time/services/auth/firebase_auth_provider.dart';

class AuthService implements AuthProvider {
  AuthProvider provider;
  AuthService({required this.provider});

  factory AuthService.firebase() =>
      AuthService(provider: FirebaseAuthProvider());

  @override
  AuthUser? get getCurrentUser => provider.getCurrentUser;

  @override
  Future<void> logout() => provider.logout();

  @override
  Future<void> initalize() => provider.initalize();

  @override
  Future<AuthUser> login(String email, String password) =>
      provider.login(email, password);

  @override
  Future<AuthUser> register(String email, String password, dispayName) =>
      provider.register(email, password, dispayName);

  @override
  Future<void> sendEmailVerifaction() => provider.sendEmailVerifaction();
}
