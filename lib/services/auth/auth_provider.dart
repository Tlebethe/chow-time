import 'package:chow_time/services/auth/auth_user.dart';

abstract class AuthProvider {
  //get current user
  AuthUser? get getCurrentUser;

  //login
  Future<AuthUser> login(String email, String password);

  //logout
  Future<void> logout();

  //register
  Future<AuthUser> register(String email, String password, String displayName);

  //initialize firebase
  Future<void> initalize();

  //send email verfication
  Future<void> sendEmailVerifaction();
}
