import 'package:chow_time/services/auth/auth_provider.dart';
import 'package:chow_time/services/auth/auth_user.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';

class UserNotFoundException implements Exception {}

class ErrorOccuredException implements Exception {}

class WrongPassWordException implements Exception {}

class UserNotLoggedInException implements Exception {}

class FirebaseAuthProvider implements AuthProvider {
  @override
  Future<void> logout() async {
    try {
      final user = FirebaseAuth.instance.currentUser;
      if (user != null) {
        await FirebaseAuth.instance.signOut();
      } else {
        throw UserNotLoggedInException();
      }
    } catch (e) {
      throw UserNotFoundException();
    }
  }

  @override
  AuthUser? get getCurrentUser {
    final user = FirebaseAuth.instance.currentUser;
    if (user != null) {
      return AuthUser.fromFirebase(user);
    } else {
      return null;
    }
  }

  @override
  Future<void> initalize() async {
    await Firebase.initializeApp();
  }

  @override
  Future<AuthUser> login(String email, String password) async {
    try {
      await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);
      final user = getCurrentUser;
      if (user != null) {
        return user;
      } else {
        throw UserNotFoundException();
      }
    } on FirebaseAuthException catch (e) {
      if (e.code == 'wrong-password') {
        throw WrongPassWordException();
      } else {
        throw ErrorOccuredException();
      }
    } catch (e) {
      throw ErrorOccuredException();
    }
  }

  @override
  Future<AuthUser> register(
      String email, String password, String displayName) async {
    try {
      final result = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password);
      final user = getCurrentUser;
      if (user != null) {
        result.user?.updateDisplayName(displayName);
        return user;
      } else {
        throw UserNotLoggedInException();
      }
    } catch (e) {
      throw ErrorOccuredException();
    }
  }

  @override
  Future<void> sendEmailVerifaction() async {
    final user = FirebaseAuth.instance.currentUser;
    if (user != null) {
      user.sendEmailVerification();
    } else {
      throw UserNotFoundException();
    }
  }
}
