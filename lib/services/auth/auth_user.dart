import 'package:firebase_auth/firebase_auth.dart';

class AuthUser {
  String? email;
  bool isVerfied;
  String? userName;

  AuthUser(
      {required this.email, required this.userName, required this.isVerfied});

  factory AuthUser.fromFirebase(User user) => AuthUser(
        email: user.email,
        userName: user.displayName,
        isVerfied: user.emailVerified,
      );
}
