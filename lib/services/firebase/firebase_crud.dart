import 'package:chow_time/models/response.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

final FirebaseFirestore _firestore = FirebaseFirestore.instance;
final CollectionReference _collection = _firestore.collection('Dish');
final user = FirebaseAuth.instance.currentUser;

class FirebaseCrud {
  //add a dish to database
  static Future<Response> addDish({
    required String mealID,
    required String strMeal,
    required String strCategory,
    required String strInstruction,
  }) async {
    Response response = Response();
    DocumentReference documentReferencer = _collection.doc();

    Map<String, dynamic> data = <String, dynamic>{
      'mealID': mealID,
      'strMeal': strMeal,
      'category': strCategory,
      'instructions': strInstruction,
    };

    var result = await documentReferencer.set(data).whenComplete(() {
      response.code = 200;
      response.message = 'Added meal success';
    }).catchError((e) {
      response.code = 500;
      response.message = e;
    });
    return response;
  }

  //read dishes from firebase
  static Stream<QuerySnapshot> readDishes() {
    CollectionReference dishesCollection = _collection;
    return dishesCollection.snapshots();
  }

  //update and edit a dish

  //delete a dish

  //update user name
  Future<void> updateUsername(String newName) async {
    if (user != null) {
      await user?.updateDisplayName(newName);
      print('name updated to ${user!.displayName} ');
    }
  }
}
