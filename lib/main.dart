import 'package:chow_time/constants/routes.dart';
import 'package:chow_time/screens/auth_screens/login.dart';
import 'package:chow_time/screens/auth_screens/register.dart';
import 'package:chow_time/screens/auth_screens/send_email_verfication.dart';
import 'package:chow_time/screens/recipe_feed.dart';
import 'package:chow_time/services/auth/auth_service.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MaterialApp(
    title: 'Chow Time',
    home: const MyHomePage(
      title: 'Chow Time',
    ),
    routes: {
      loginView: ((context) => const Login()),
      registerView: (context) => const RegisterScreen(),
      feedView: ((context) => const RecipeFeed()),
      verifyEmail: (context) => const SendEmailVerification(),
      //recipeDetail: (context) => const RecipeDetailScreen(),
    },
    theme: ThemeData(
        // primarySwatch: Colors.blue,
        // textTheme: GoogleFonts.poppinsTextTheme(),
        ),
  ));
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: AuthService.firebase().initalize(),
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.done:
            final user = AuthService.firebase().getCurrentUser;
            if (user != null) {
              if (user.isVerfied) {
                return const RecipeFeed();
              } else {
                return const SendEmailVerification();
              }
            } else {
              return const Login();
            }

          default:
            return const CircularProgressIndicator();
        }
      },
    );
  }
}
