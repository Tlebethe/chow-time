class Dish {
  String? idMeal;
  String? strMeal;
  String? strCategory;
  String? strInstructions;
  String? strMealThumb;
  List<String> ingredients = [];
  List<String> strMeasure = [];

  Dish({
    required this.idMeal,
    required this.strMeal,
    required this.strCategory,
    required this.strInstructions,
    required this.strMealThumb,
  });

  void addIngredients(String ingredient) {
    ingredients.add(ingredient);
  }

  void addMeasure(String ingredient) {
    ingredients.add(ingredient);
  }

  List<String> get getIngredients => ingredients;
  List<String> get getMeasure => strMeasure;
}
