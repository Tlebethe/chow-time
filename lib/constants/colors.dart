import 'dart:ui';

const coolGreen = Color.fromRGBO(67, 146, 112, 1.0);
const coolGrey = Color.fromRGBO(69, 69, 69, 1.0);
const coolWhite = Color.fromRGBO(255, 255, 255, 1.0);
