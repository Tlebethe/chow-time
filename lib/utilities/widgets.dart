import 'package:cached_network_image/cached_network_image.dart';
import 'package:chow_time/screens/recipe_detail_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../constants/colors.dart';
import '../models/favorite_recipe_model.dart';

Widget mealCard(
  BuildContext context,
  String mealName,
  String imageLink,
  String id,
  RecipeModel recipeModel,
) {
  return InkWell(
    onTap: () {
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: ((context) => ChangeNotifierProvider.value(
                value: recipeModel,
                child: RecipeDetailScreen(
                  id: id,
                ),
              )),
        ),
      );
    },
    child: Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        color: Colors.black,
        borderRadius: BorderRadius.circular(10),
        image: DecorationImage(
          opacity: 0.5,
          image: NetworkImage(imageLink),
          fit: BoxFit.cover,
        ),
      ),
      height: 150,
      margin: const EdgeInsets.only(
        top: 15,
        bottom: 10,
      ),
      child: Padding(
        padding: const EdgeInsets.all(25.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Row(
                  children: const [
                    Text(
                      '4.8',
                      style: TextStyle(color: coolWhite, fontSize: 18),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Icon(
                      Icons.favorite,
                      color: coolWhite,
                      size: 25,
                    )
                  ],
                )
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              mealName,
              overflow: TextOverflow.ellipsis,
              style: const TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
                color: coolWhite,
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

Widget categoryBarItem(
  String discription,
  Function changeView,
  bool isChoosen,
  Function changeChoosen,
  int index,
) {
  return Container(
    decoration: BoxDecoration(
      borderRadius: const BorderRadius.all(Radius.circular(20)),
      color: !isChoosen ? Colors.black12 : coolGrey,
    ),
    margin: const EdgeInsets.all(10),
    child: TextButton(
      onPressed: () async {
        changeView(discription);
        changeChoosen(index);
      },
      child: Text(
        discription,
        style: TextStyle(
          fontSize: 15,
          color: !isChoosen ? coolGreen : coolWhite,
        ),
      ),
    ),
  );
}

Widget ingredientWidget({
  required BuildContext context,
  required ingridentItem,
  required ingredientMeasure,
}) {
  return Column(
    children: [
      const SizedBox(
        height: 25,
      ),
      //ingredient row
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          //left side - pic , ingredient
          Row(
            children: [
              Container(
                height: 40,
                width: 40,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: coolWhite,
                ),
                child: CachedNetworkImage(
                  // placeholder: (context, url) => const Text('Loading image...'),
                  progressIndicatorBuilder: (context, url, progress) => Center(
                    child: CircularProgressIndicator(
                      value: progress.progress,
                    ),
                  ),
                  imageUrl:
                      'https://www.themealdb.com/images/ingredients/$ingridentItem.png',

                  fit: BoxFit.cover,
                  errorWidget: (context, url, error) =>
                      const Text('loading...'),
                ),
              ),
              const SizedBox(
                width: 20,
              ),
              Text(
                ingridentItem ?? '',
                style: const TextStyle(
                  fontSize: 20,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ],
          ),
          //right side - 3pc
          Text(
            ingredientMeasure ?? 'no data',
            style: const TextStyle(
              fontSize: 20,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ],
      )
    ],
  );
}

Widget favoriteMealCard(
  BuildContext context,
  String mealName,
  String imageLink,
  String id,
  RecipeModel recipeModel,
) {
  return InkWell(
    onTap: () {
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: ((context) => ChangeNotifierProvider.value(
                value: recipeModel,
                child: RecipeDetailScreen(
                  id: id,
                ),
              )),
        ),
      );
    },
    child: Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        color: Colors.black,
        borderRadius: BorderRadius.circular(10),
        image: DecorationImage(
          opacity: 0.5,
          image: NetworkImage(imageLink),
          fit: BoxFit.cover,
        ),
      ),
      height: 150,
      margin: const EdgeInsets.only(
        top: 15,
        bottom: 10,
      ),
      child: Padding(
        padding: const EdgeInsets.all(25.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Row(
                  children: [
                    const SizedBox(
                      width: 10,
                    ),
                    IconButton(
                      onPressed: () {
                        recipeModel.removeDish(id);
                      },
                      icon: const Icon(
                        Icons.delete,
                        color: coolWhite,
                        size: 25,
                      ),
                    ),
                  ],
                )
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              mealName,
              overflow: TextOverflow.ellipsis,
              style: const TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
                color: coolWhite,
              ),
            ),
          ],
        ),
      ),
    ),
  );
}
